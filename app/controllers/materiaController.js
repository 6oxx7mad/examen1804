const Materia = require('../models/Materia')
const { ObjectId } = require('mongodb')

const index = (req, res) => {
  Materia.find((err, materias) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo  la lista de Materias'
      })
    }
    return res.json(materias)
  })
}

const create = (req, res) => {
  const materia = new Materia(req.body)
  materia.save((err, materia) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar el Materia',
        error: err
      })
    }
    return res.status(201).json(materia)
  })
}

const show = (req, res) => {
  const id = req.params.id
  Materia.findById(id, (err, materia) => {
    if (err) return res.status(500).json({ message: 'error' })
    if (!materia) return res.status(404).json({ message: 'not found' })
    return res.json(materia)
  })
}

const destroy = (req, res) => {
  const id = req.params.id
  Materia.findByIdAndDelete(id, (err, data) => {
    if (err) return res.status(500).json({ message: 'error' })
    return res.json(data)
  })
}

const update = (req, res) => {
  const id = req.params.id
  Materia.findOne({ _id: id }, (err, materia) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar el Materia',
        error: err
      })
    }
    if (!materia) {
      return res.status(404).json({
        message: 'No hemos encontrado el Materia'
      })
    }

    Object.assign(materia, req.body)

    materia.save((err, materia) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar el Materia'
        })
      }
      if (!materia) {
        return res.status(404).json({
          message: 'No hemos encontrado el Materia'
        })
      }
      return res.json(materia)
    })
  })
}

module.exports = {
  index,
  create,
  show,
  destroy,
  update
}
