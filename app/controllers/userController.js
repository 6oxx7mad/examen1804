const User = require('../models/User')
const { ObjectId } = require('mongodb')
const servicejwt = require('../services/servicejwt')

const login = (req, res) => {
    User.find({ nombre: req.body.name, password: req.body.password }, function (err, user) {
        if (err) return res.status(500).json({ message: 'error' })
        if (!user) return res.status(404).json({ message: 'not found' })
        //console.log(servicejwt.decodeToken(servicejwt.createToken(user)))
        return res.status(200).send({ token: servicejwt.createToken(user) })
    });
}

module.exports = {
    login
}