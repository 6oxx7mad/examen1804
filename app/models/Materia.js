const mongoose = require('mongoose')
const Schema = mongoose.Schema

const materiaSchema = new Schema({
  codigo: {
    type: String,
    unique: true,
    required: true
  },
  nombre: String,
  curso: String,
  horas: Number
})

const Materia = mongoose.model('Materia', materiaSchema)

module.exports = Materia
