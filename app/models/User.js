const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  codigo: {
    type: String,
    unique: true,
    required: true
  },
  nombre: String,
  password: String
})

const User = mongoose.model('User', userSchema)

module.exports = User
